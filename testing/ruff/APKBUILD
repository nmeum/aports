# Maintainer: Hugo Osvaldo Barrera <hugo@whynothugo.nl>
pkgname=ruff
pkgver=0.3.2
pkgrel=0
pkgdesc="Extremely fast Python linter"
url="https://github.com/astral-sh/ruff"
# armhf: tests fail
# armv7: tests fail
# s390x: nix crate
# x86: tests fail
arch="all !x86 !armhf !armv7 !s390x"
license="MIT"
makedepends="py3-gpep517 py3-maturin cargo py3-installer"
subpackages="
	$pkgname-pyc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/astral-sh/ruff/archive/v$pkgver/ruff-$pkgver.tar.gz"
# net: cargo
options="net"

export CARGO_PROFILE_RELEASE_OPT_LEVEL=2

prepare() {
	default_prepare

	# shadow git repo for tests
	git init -q

	# Avoid downloading a different toolchain on systems with rustup installed.
	rm rust-toolchain.toml

	cargo fetch --locked
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--config-json '{"build-args": "--frozen"}' \
		--output-fd 3 3>&1 >&2

	./target/release/ruff --generate-shell-completion bash > $pkgname.bash
	./target/release/ruff --generate-shell-completion fish > $pkgname.fish
	./target/release/ruff --generate-shell-completion zsh > $pkgname.zsh

	# Update ruff.schema.json as the pre-built one is generated
	# using the '--all-features' Cargo flag which we don't pass.
	cargo dev generate-all
}

check() {
	unset CI_PROJECT_DIR

	cargo test --frozen
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl

	install -Dm644 $pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/$pkgname.fish
	install -Dm644 $pkgname.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
3505e199d631bb951c89814e7516e4a20dda602f454ce3f122902455fe07df38195605db92350543463aa60aa2558a8dccd3d68db09cc73c7682b705b90c03c7  ruff-0.3.2.tar.gz
"
